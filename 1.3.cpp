#include <iostream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

void printPascal(int rows);			// print the pascal triangle with rows rows
void printWhiteSpace(int k);		// print k consecutive white spaces

void printWhiteSpace(int k) {
	for (int i = 0; i < k; i++)
		cout << ' ';
}

void printPascal(int rows) {
	vector<vector<int>> res;
	int brick = 1;		// brick size
	if (rows == 0)
		return;

	// initiate the first row
	res.push_back(vector<int>(1, 1));

	// construct the pascal triangle
	for (int i = 1; i < rows; i++) {
		vector<int> newRow;
		newRow.push_back(1);
		for (int j = 1; j < i; j++) {
			newRow.push_back(res[i - 1][j - 1] + res[i - 1][j]);
			//update the brick size
			brick = (int)to_string(newRow[j]).size() > brick ? (int)to_string(newRow[j]).size() : brick;
		}
		newRow.push_back(1);
		res.push_back(newRow);
	}
	//set the brick size as an odd number
	brick = (brick % 2 == 1) ? brick : brick + 1;

	//print all the rows
	for (int i = 0; i < rows; i++) {
		printWhiteSpace(brick * (rows - i - 1));
		for (int j = 0; j < res[i].size(); j++) {
			int numberSize = to_string(res[i][j]).size();
			printWhiteSpace((brick - numberSize) / 2);
			cout << res[i][j];
			printWhiteSpace(brick - numberSize - (brick - numberSize) / 2);
			printWhiteSpace(brick);
		}
		cout << endl;
	}
	cout << brick;
	return;
}


int main(int argc, char** argv) {
	int rows = atoi(argv[1]);
	printPascal(rows);
	return 0;
}