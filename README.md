# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is my solutions to homework 1 in the course C++.


### hw 1.1 ###

1.1_frame.png and 1.1_frame2.png are screenshots showing that I have run "Frame" programs successfully.


### hw 1.2 ###

1.2.png is a screenshot showing that I have run the sample optimized copy program successfully.


### hw 1.3 ###

1.3.cpp is the source code of my solution to hw 1.3.
It is dynamic and can be run in this way:
./Pascal 12


### hw 1.3 extra credit ###

1.3_extra.cpp is the source code for hw 1.3 - Extra Credit. It is formatted in "brick-wall" formatting.
It is dynamic and can be run in this way:
./Pascal 12


### hw 1.4 ###

1.4.cpp is a program which is a valid C program but not a valid C++ program. That's because "template" is not allowed to be used as an identifier in C++, but it's allowed in C.


### hw 1.5 ###

1.5.txt is my explanation why C++ is called C++ and not ++C.