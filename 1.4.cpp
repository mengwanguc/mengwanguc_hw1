#include <stdio.h>
#include <stdlib.h>

int main() {
	// 'template' is not allowed to be used as an identifier in C++
	// But it is allowed in C
	int template = 12345;
	printf("%d\n", template);
	return 0;
}